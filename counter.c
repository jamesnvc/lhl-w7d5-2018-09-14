// To compile:
// clang -pthread -O2 counter.c -o counter
// then run with ./counter
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

static int data[] = {
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9
};

static int total_count = 0;

static pthread_mutex_t total_mutex;

struct range {
  int start;
  int len;
};

void* count_words(void *arg) {
  struct range *range = (struct range*)arg;
  int start = range->start;
  int len = range->len;
  for (int i = 0; i < len; i++) {
    usleep(rand() % 5000);
    pthread_mutex_lock(&total_mutex);
    total_count += data[start + i];
    pthread_mutex_unlock(&total_mutex);
  }
  return NULL;
}

int main(int argc, char** argv) {

  pthread_mutex_init(&total_mutex, NULL);

  pthread_t thread_1, thread_2, thread_3, thread_4, thread_5;

  struct range arg_1 = { 0, 10 };
  struct range arg_2 = { 10, 10 };
  struct range arg_3 = { 20, 10 };
  struct range arg_4 = { 30, 10 };
  struct range arg_5 = { 40, 10 };

  pthread_create(&thread_1, NULL, count_words, &arg_1);
  pthread_create(&thread_2, NULL, count_words, &arg_2);
  pthread_create(&thread_3, NULL, count_words, &arg_3);
  pthread_create(&thread_4, NULL, count_words, &arg_4);
  pthread_create(&thread_5, NULL, count_words, &arg_5);

  pthread_join(thread_1, NULL);
  pthread_join(thread_2, NULL);
  pthread_join(thread_3, NULL);
  pthread_join(thread_4, NULL);
  pthread_join(thread_5, NULL);

  pthread_mutex_destroy(&total_mutex);

  printf("Total is %d\n", total_count);

  return 1;
}
