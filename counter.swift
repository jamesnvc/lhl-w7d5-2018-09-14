import Foundation

var total = 0
let data = [
  0,1,2,3,4,5,6,7,8,9,
  0,1,2,3,4,5,6,7,8,9,
  0,1,2,3,4,5,6,7,8,9,
  0,1,2,3,4,5,6,7,8,9,
  0,1,2,3,4,5,6,7,8,9,
]

let conQueue = DispatchQueue(
    label: "com.occasionallycogent.concurrent",
    qos: .default,
    attributes: [.concurrent]
)

let serialQueue = DispatchQueue(
    label: "com.occasionallycogent.serial",
    qos: .default,
    attributes: [] // serial is default
)

let group = DispatchGroup()

for i in 0...4 {
    group.enter()
    conQueue.async {
        // Stuff in here can execute simultaneously with other
        // "threads" (i.e. the other copies of this block that are
        // being dispatched to the concurrent queue)
        print("thread \(i) start")
        let start = i * 10
        let end = start + 10
        let sum = data[start..<end].reduce(0, +)
        print("thread \(i) work done")
        serialQueue.async {
            // stuff in here is only going to execute one block at a
            // time
            print("adding total start \(i)")
            total += sum
            print("adding total done \(i)")
            group.leave()
        }
    }
}

/*
group.notify(queue: DispatchQueue.main) {
    print("Total is \(total)")
}
*/

// group.wait() is like pthread_join: it means wait here until every
// group.enter() has been balanced by a group.leave()
group.wait()
print("total is \(total)")
